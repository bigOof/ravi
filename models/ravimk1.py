from gtts import gTTS
import speech_recognition as sr
import re
import time
import webbrowser
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import smtplib
import requests
from pygame import mixer
import urllib.request
import urllib.parse
import json
import bs4
import playsound 


r =sr.Recognizer()
mic =sr.Microphone()
harvard = sr.AudioFile('harvard.wav')

def speak(text):
    tts = gTTS(text=text, lang='en' ,slow=True)
    filename = 'voice.mp3'
    tts.save(filename)
    playsound.playsound(filename)


def get_audio():
    audio = r.listen(source)
    r.recognize_google(audio)



